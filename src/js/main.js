//= ../../node_modules/jquery/dist/jquery.min.js
//= ../../node_modules/owl.carousel/dist/owl.carousel.min.js
//= ../../node_modules/zepto/dist/zepto.min.js
//= ../../node_modules/gsap/dist/gsap.min.js
//= ../../node_modules/scrollmagic/scrollmagic/minified/ScrollMagic.min.js
//= ../../node_modules/scrollmagic/scrollmagic/minified/plugins/animation.gsap.min.js
//= hero.js
let tl = new TimelineMax();

let init = () =>
    tl.fromTo('.logo', 3, {scale: 0.01, y: 0, opacity: 0}, {
        scale: 1,
        y: 0,
        opacity: 1,
        ease: Back.easeOut.config(3)
    });

window.addEventListener('load', (event) => {
    init();
});

// document.getElementById('year').innerHTML = new Date().getFullYear();


$(document).ready(function () {
    $('.owl-carousel').owlCarousel({
        items: 1,
        dotsContainer: '#carousel-hash',
        autoplay: 3000, // time for slides changes
        smartSpeed: 1000, // duration of change of 1 slide
        loop: true,

    });

    let $hamburger = $('.hamburger');
    let $navMenu = $('.nav-menu nav');
    $hamburger.on('click', function (e) {
        $hamburger.toggleClass('is-active');
        $navMenu.toggleClass('is-active');
    });

    let $headerNavItem = $('.nav-menu nav a');
    $($headerNavItem).bind('click', function (e) {
        e.preventDefault();
        var target = $(this).attr('href');
        $('html, body').stop().animate({
            scrollTop: $(target).offset().top
        }, 600);
        return false;
    });
    $(window).scroll(function () {
        var scrollDistance = $(window).scrollTop();
        $('.section-wrapper').each(function (i) {
            if ($(this).position().top <= scrollDistance) {
                $($headerNavItem).removeClass('active');
                $($headerNavItem).eq(i).addClass('active');
            }
        });
    }).scroll();

    // Init ScrollMagic
    let controller = new ScrollMagic.Controller();

// FadeInBottom
    let fadeElem = Array.prototype.slice.call(document.querySelectorAll(".scroll-animate"));
    let self = this;

    fadeElem.forEach(function (self) {
        // build a tween
        let fadeInBottom = TweenMax.from(self, 1.5, {y: 100, opacity: 0});
        // build a scene
        let scene = new ScrollMagic.Scene({
            triggerElement: self,
            offset: -200,
            reverse: false
        })
            .setTween(fadeInBottom)
            .addTo(controller)
    })

    let map = $('.contacts__map');
    map.behaviors.disable('drag');
});
